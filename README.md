# HowLongToBeat Provider Library

## Usage

### Install the dependency

```
npm install hltb-provider 
```

### Import the library

#### TypeScript
```TypeScript
import { HLTBProvider } from 'hltb-provider';
```

#### JavaScript
```JavaScript
var hltb = require('hltb-provider')
```

### Search for a game (Limited to 10 first results)

#### TypeScript
```TypeScript
let provider = new HLTBProvider();

provider.search("Persona 4").then(result => {
    console.log(result);
})
```

#### JavaScript
```JavaScript
var provider = new hltb.HLTBProvider();

provider.search("Persona 4").then(result => {
    console.log(result)
})
```

### Search Results
```JavaScript
[ HLTBSearchEntry {
    id: '6974',
    name: 'Persona 4: Golden',
    imageUrl: 'https://howlongtobeat.com/gameimages/persona_4_golden_large.jpg' },
  HLTBSearchEntry {
    id: '8443',
    name: 'Shin Megami Tensei: Persona 4',
    imageUrl: 'https://howlongtobeat.com/gameimages/Shin_Megami_Tensei_Persona_4.jpg' },
  HLTBSearchEntry {
    id: '6973',
    name: 'Persona 4 Arena',
    imageUrl: 'https://howlongtobeat.com/gameimages/Persona4Arena.png' },
  HLTBSearchEntry {
    id: '27669',
    name: 'Persona 4: Dancing All Night',
    imageUrl: 'https://howlongtobeat.com/gameimages/Persona_4_Dancing_All_Night_cover.png' },
  HLTBSearchEntry {
    id: '21257',
    name: 'Persona 4 Arena Ultimax',
    imageUrl: 'https://howlongtobeat.com/gameimages/Persona_4_Arena_Ultimax.png' } ]

```

### Get game details

#### TypeScript
```TypeScript
let provider = new HLTBProvider();

provider.detail("6974").then(result => {
    console.log(result);
})
```

#### JavaScript
```JavaScript
var provider = new hltb.HLTBProvider();

provider.detail("6974").then(result => {
    console.log(result)
})
```

### Detail Results
```JavaScript
HLTBDetailEntry {
  id: '6974',
  name: 'Persona 4: Golden',
  otherData:
   OtherInfo {
     imageUrl: 'https://howlongtobeat.com/gameimages/persona_4_golden_large.jpg',
     developer: 'Atlus',
     publisher: 'Atlus',
     platforms: [ 'PlayStation Vita' ] },
  timeData: TimeInfo { gameplayTime: 69.5, gameplayTimeComplete: 142 } }
```

## Known Issues

## About & Credits

[hltb-provider](https://gitlab.com/alosarjos/hltb-provider) provides basic functionality for parsing HowLongToBeat games information.

Credits to: [ckatzorke](https://github.com/ckatzorke/howlongtobeat) for providing me an excelent starting point for this project
