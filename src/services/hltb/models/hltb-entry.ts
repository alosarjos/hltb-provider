/**
 * Class for the info for each search result
 */
export class HLTBSearchEntry {
    private id: string;
    private name: string;
    private imageUrl: string;

    constructor($id: string, $name: string, $imageUrl: string) {
        this.id = $id;
        this.name = $name;
        this.imageUrl = $imageUrl;
    }

    public get $id(): string {
        return this.id;
    }

    public get $name(): string {
        return this.name;
    }

    public get $imageUrl(): string {
        return this.imageUrl;
    }
}

/**
 * Class for the details  of each game
 */
export class HLTBDetailEntry {
    private id: string;
    private name: string;
    private timeData: TimeInfo;
    private otherData: OtherInfo;

    constructor($id: string, $name: string, $otherInfo: OtherInfo, $timeData: TimeInfo) {
        this.id = $id;
        this.name = $name;
        this.otherData = $otherInfo;
        this.timeData = $timeData;
    }

    public get $id(): string {
        return this.id;
    }

    public get $name(): string {
        return this.name;
    }

    public get $otherInfo(): OtherInfo {
        return this.otherData;
    }

    public get $timeData(): TimeInfo {
        return this.timeData;
    }
}

/**
 * Time info inside HLTBDetailEntry
 */
export class TimeInfo {
    private gameplayTime: number;
    private gameplayTimeComplete: number;

    constructor($gameplayTime: number, $gameplayTimeComplete: number) {
        this.gameplayTime = $gameplayTime;
        this.gameplayTimeComplete = $gameplayTimeComplete;
    }

    public get $gameplayTime(): number {
        return this.gameplayTime;
    }

    public get $gameplayTimeComplete(): number {
        return this.gameplayTimeComplete;
    }
}

/**
 * Other info inside HLTBDetailEntry
 */
export class OtherInfo {
    private imageUrl: string
    private developer: string;
    private publisher: string;
    private platforms: Array<string>;

    constructor($imageUrl: string, $developer: string, $publisher: string, $platforms: Array<string>) {
        this.imageUrl = $imageUrl;
        this.developer = $developer;
        this.publisher = $publisher;
        this.platforms = $platforms;
    }

    public get $imageUrl(): string {
        return this.imageUrl;
    }

    public get $developer(): string {
        return this.developer;
    }

    public get $publisher(): string {
        return this.publisher;
    }

    public get $platforms(): Array<string> {
        return this.platforms;
    }
}

