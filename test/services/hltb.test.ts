import { expect } from 'chai';

import { HLTBSearchEntry, HLTBDetailEntry } from '../../src/services/hltb/models/hltb-entry';
import { HLTBProvider } from '../../src/services/hltb/providers/hltb-provider';

let provider = new HLTBProvider();
let resultArray: Array<HLTBSearchEntry>;
let resultElement: HLTBDetailEntry;

describe('HowLongToBeat provider search() method', () => {

    it('Should return a HowLongToBeatEntry array', () => {
        let result: Promise<Array<HLTBSearchEntry>> = new Promise<Array<HLTBSearchEntry>>((resolve, reject) => {
            provider.search("Persona 4").then(result => {
                expect(result).to.be.an('array');
                expect(result).to.not.be.empty;
                expect(result).to.have.lengthOf(5);
                resultArray = result;
                resolve(result)
            });
        })
        return result;
    }).timeout(100000);

    it('Should have the Persona 4 partial element in the array', () => {
        expect(resultArray[0].$id).to.equal('8443')
        expect(resultArray[0].$name).to.equal('Shin Megami Tensei: Persona 4');
        expect(resultArray[0].$imageUrl).to.equal('https://howlongtobeat.com/gameimages/Shin_Megami_Tensei_Persona_4.jpg');
    })
});


describe('HowLongToBeat provider detail() method', () => {
    it('Should return a HowLongToBeatEntry element', () => {
        let result: Promise<HLTBDetailEntry> = new Promise<HLTBDetailEntry>((resolve, reject) => {
            provider.detail(resultArray[0].$id).then(result => {
                expect(result).not.to.be.empty;
                resultElement = result;
                resolve(result)
            });
        })
        return result;
    }).timeout(100000);

    it('Should be the Persona 4 HowLongToBeatEntry element', () => {
        expect(resultElement.$id).to.equal('8443')
        expect(resultElement.$name).to.equal('Shin Megami Tensei: Persona 4');
        expect(resultElement.$otherInfo.$imageUrl).to.equal('https://howlongtobeat.com/gameimages/Shin_Megami_Tensei_Persona_4.jpg');
        expect(resultElement.$otherInfo.$developer).to.equal('Atlus');
        expect(resultElement.$otherInfo.$publisher).to.equal('Atlus, Square Enix');
        expect(resultElement.$otherInfo.$platforms).to.have.lengthOf(4);
        expect(resultElement.$otherInfo.$platforms[0]).to.equal('PC');
        expect(resultElement.$timeData.$gameplayTime).to.equal(73);
        expect(resultElement.$timeData.$gameplayTimeComplete).to.equal(117);
    });
})
