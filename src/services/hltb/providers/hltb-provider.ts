import { HLTBSearchEntry, HLTBDetailEntry } from '../models/hltb-entry';
import { HLTBConnector } from './hltb-connector';
import { HLTBParser } from './hltb-parser'

/**
 * Class in charge of providing public methods
 */
export class HLTBProvider {
    private hltbConnector: HLTBConnector;

    constructor() {
        this.hltbConnector = new HLTBConnector();
    }

    /**
     * Search through the HLTB searching website.
     * @param gameName Name to search
     */
    public async search(gameName: string): Promise<Array<HLTBSearchEntry>> {
        let searchPage = await this.hltbConnector.search(gameName);
        let result = HLTBParser.parseSearch(searchPage);
        return result;
    }

    /**
     * 
     * @param gameId Game ID to retrieve it's detailts
     */
    public async detail(gameId: string): Promise<HLTBDetailEntry> {
        let detailRawData = await this.hltbConnector.detail(gameId);
        let entry = HLTBParser.parseDetails(detailRawData);
        return entry;
    }
}