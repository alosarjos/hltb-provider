import * as cheerio from 'cheerio';
import { HLTBConnector } from './hltb-connector';
import { HLTBSearchEntry, HLTBDetailEntry, OtherInfo, TimeInfo } from '../models/hltb-entry'

const BASE_URL: string = 'https://howlongtobeat.com/';

/**
 * Data parsers class
 */
export class HLTBParser {
    /**
     * Details data parser
     * @param jsonString JSON in string format to parse
     * @param id ID of the game
     */
    static parseDetails(htmlString: string): HLTBDetailEntry {
        let gameId = '';
        let gameName = '';
        let imageUrl = '';
        let developer = '';
        let publisher = '';
        let platforms = new Array<string>();
        let gameplayMain = 0;
        let gameplayComplete = 0;

        const $ = cheerio.load(htmlString);

        let gameEntry = $('.profile_info');

        gameId = $('.profile_nav').children().first().attr('href').substring($('.profile_nav').children().first().attr('href').indexOf('?id=') + 4);
        gameName = $('.profile_header').text().trim();
        imageUrl = $('.game_image img').attr('src').trim();
        developer = gameEntry.eq(0).text().trim().substring(gameEntry.eq(0).text().trim().indexOf('\n')).trim();
        publisher = gameEntry.eq(1).text().trim().substring(gameEntry.eq(1).text().trim().indexOf('\n')).trim();
        platforms = gameEntry.eq(2).text().trim().substring(gameEntry.eq(2).text().trim().indexOf('\n')).trim().split(',').map(string => {
            return string.trim();
        });

        let liElements = $('.game_times li');
        liElements.each((i, li) => {
            let type: string = $('.game_times li').eq(i).find('h5').eq(0).text().trim();
            let time: number = HLTBParser.parseTime($('.game_times li').eq(i).find('div').text().trim());
            if (type.startsWith('Main Story') || type.startsWith('Single-Player') || type.startsWith('Solo')) {
                gameplayMain = time;
            } else if (type.startsWith('Completionist')) {
                gameplayComplete = time;
            }
        });
        return new HLTBDetailEntry(gameId, gameName, new OtherInfo(imageUrl, developer, publisher, platforms), new TimeInfo(gameplayMain, gameplayComplete));
    }

    /**
     * Search data parser
     * @param html HTML website to parse
     */
    static parseSearch(html: string): Array<HLTBSearchEntry> {
        let results: Array<HLTBSearchEntry> = new Array<HLTBSearchEntry>();
        let main = 0;
        let complete = 0;
        const $ = cheerio.load(html);

        if ($('h3').length > 0) {
            let liElements = $('li');
            liElements.each((i, element) => {

                //Get game details
                let gameName = $('li').eq(i).find('a').attr('title').trim();
                let detailId = $('li').eq(i).find('a').attr('href').substring($('li').eq(i).find('a').attr('href').indexOf('?id=') + 4)
                let imageUrl = $('li').eq(i).find('img').attr('src').trim()

                results.push(new HLTBSearchEntry(detailId, gameName, imageUrl))
            })
        }
        return results;
    }

    private static parseTime(text: string): number {
        if (text === '--') {
            return 0;
        }
        if (text.indexOf(' - ') > -1) {
            return HLTBParser.handleRange(text);
        }
        return HLTBParser.getTime(text);
    }

    private static handleRange(text: string): number {
        let range: Array<string> = text.split(' - ');
        let d: number = (HLTBParser.getTime(range[0]) + HLTBParser.getTime(range[1])) / 2;
        return d;
    }

    private static getTime(text: string): number {
        let time: string = text.substring(0, text.indexOf(" "));
        if (time.indexOf('½') > -1) {
            return 0.5 + parseInt(time.substring(0, text.indexOf('½')));
        }
        return parseInt(time);
    }
}


