import * as https from 'https';
import { IncomingMessage } from 'http';
import * as querystring from 'querystring';

import { HttpRequestOptions } from '../../shared/connector';

/**
 * Parameters for the detail request
 */
const detailHttpRequestOptions = {
    hostname: 'howlongtobeat.com',
    port: 443,
    method: 'GET',
    path: '/game.php?',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'accept': '*/*'
    }
}

/**
 * Parameters for the search request
 */
const searchHttpRequestOptions = {
    hostname: 'howlongtobeat.com',
    port: 443,
    method: 'POST',
    path: '/search_main.php?page=1',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'accept': '*/*'
    }
}

/**
 * Class in charge of doing the net requests
 */
export class HLTBConnector {
    private requestOptions: HttpRequestOptions;

    constructor() {
        this.requestOptions = new HttpRequestOptions();
    }

    /**
     * Write the data chunks into a single string
     * @param res HTTPS response
     * @param resolve Function to finish
     */
    static async writeDataChunk(res: IncomingMessage, resolve) {
        if (res.statusCode == 200) {
            res.setEncoding('utf8');
            let rawData = '';
            res.on('data', (chunk) => { rawData += chunk; });
            res.on('end', () => {
                try {
                    resolve(rawData);
                } catch (e) {
                    console.error(e.message);
                }
            });
        }
    }

    /**
     * Set the request options and do the connection for details
     * @param id Game ID of the target
     */
    async detail($id: string): Promise<string> {
        this.requestOptions.setOptions(detailHttpRequestOptions);
        this.requestOptions.$path = querystring.stringify({ id: $id })
        let result: Promise<string> = new Promise<string>((resolve, reject) => {
            https.get(this.requestOptions.getOptionsObject(), res => {
                HLTBConnector.writeDataChunk(res, resolve);
            })
        });
        return result;
    }

    /**
     * Set the request options and do the connection for search
     * @param gameName Game name to do the search
     */
    async search(gameName: string): Promise<string> {
        this.requestOptions.setOptions(searchHttpRequestOptions);
        let postData = querystring.stringify({
            queryString: gameName,
            t: 'games',
            sorthead: 'popular',
            sortd: 'Normal Order',
            plat: '',
            length_type: 'main',
            length_min: '',
            length_max: '',
            detail: '0'
        })

        let result: Promise<string> = new Promise<string>((resolve, reject) => {
            let req = https.request(this.requestOptions.getOptionsObject(), res => {
                HLTBConnector.writeDataChunk(res, resolve);
            })
            req.write(postData);
            req.end();
        })
        return result;
    }
}
